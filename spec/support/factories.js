const assert = require('assert-plus')
const ApiKey = require('../../src/models/api_key.js')
const ApiKeyLimiter = require('../../src/models/api_key_limiter.js')

/* NOTE: Declare your factories here.
 *
 */

const Factories = {
  createApiKey: async (attrs = {}) => {
    const {
      key = 'FAKE_API_KEY'
    } = attrs

    return ApiKey.create({ key })
  },

  createApiKeyLimiter: async (attrs = {}) => {
    const {
      api_key_id = (await Factories.createApiKey()).id,
      start_count_at = null,
      num_calls = 0,
    } = attrs

    return ApiKeyLimiter.create({ api_key_id, start_count_at, num_calls })
  }
}

module.exports = Factories
