const nock = require('nock')
const Jasmine = require('jasmine')
const Mongo = require('./mongo.js')

const jasmine_env = new Jasmine()

jasmine_env.loadConfigFile('./spec/support/jasmine.json')

jasmine_env.onComplete(have_all_tests_passed => {
  if (have_all_tests_passed) {
    return process.exit(0)
  } else {
    return process.exit(1)
  }
})

Mongo.connect()
  .then(() => {
    nock.disableNetConnect()

    /* NOTE: Allow localhost connections in order to enable testing of local routes.
     */
    nock.enableNetConnect('127.0.0.1')

    jasmine_env.execute()
  })
  .catch(err => {
    console.error(err)
    process.exit(1)
  })
