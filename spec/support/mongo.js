const assert = require('assert-plus')
const mongoose = require('mongoose')
const DatabaseCleaner = require('database-cleaner')

const self = { client: null }

const MongoHelpers = {
  connect: async () => {
    if (!self.client) {
      /* TODO: Un-hardcode the config values.
       */

      const user = 'root'
      const password = 'pretendimasecurepasswordformongotest'
      const host = 'mongo_test'
      const port = '27017'

      const conn_url = `mongodb://${user}:${password}@${host}:${port}`

      const conn_opts = {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false
      }

      await mongoose.connect(conn_url, conn_opts)

      self.client = mongoose.connection
    }

    return self.client
  },

  clean: async () => {
    return new Promise((resolve, reject) => {
      const db_cleaner = new DatabaseCleaner('mongodb')

      db_cleaner.clean(self.client.db, err => {
        if (err) {
          reject(err)
          return
        }

        resolve()
      })
    })
  },

  tryCastStringToObjectId: s => {
    assert.string(s, 's')

    try {
      return new mongoose.Types.ObjectId(s)
    } catch (_err) {
      return null
    }
  }
}

module.exports = MongoHelpers
