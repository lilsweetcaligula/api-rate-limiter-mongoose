const assert = require('assert-plus')

exports.isUuidV4 = x => {
  /* NOTE: Copied from here: https://gist.github.com/johnelliott/cf77003f72f889abbc3f32785fa3df8d
   */

  const v4 = new RegExp(
    /^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i
  )

  return typeof x === 'string' && v4.test(x)
}

