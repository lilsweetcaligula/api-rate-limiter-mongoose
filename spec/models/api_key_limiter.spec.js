const assert = require('assert-plus')
const ApiKeyLimiter = require('../../src/models/api_key_limiter.js')

describe('ApiKeyLimiter', () => {
  describe('updateForApiKey', () => {
    it('defaults the options to an empty object', async () => {
      spyOn(ApiKeyLimiter, 'findOneAndUpdate')

      const api_key = { _fake: 'api key doc' }
      const default_opts = {}

      await ApiKeyLimiter.updateForApiKey(api_key, {}, {})

      expect(ApiKeyLimiter.findOneAndUpdate).toHaveBeenCalledWith(
        jasmine.any(Object),
        jasmine.any(Object),
        default_opts
      )
    })
  })
})

