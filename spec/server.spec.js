const assert = require('assert-plus')
const http = require('http')
const App = require('../src/app.js')
const Mongo = require('../src/lib/mongo.js')
const Server = require('../src/server.js')
const config = require('../src/config.js')

describe('server', () => {
  let fake_server, fake_app

  const some_server_port = 3333
  const some_server_host = '7.7.7.7'

  const some_mongo_user = 'abcd'
  const some_mongo_password = '1234'
  const some_mongo_host = 'xyz'
  const some_mongo_port = '1234'

  beforeEach(() => {
    fake_server = jasmine.createSpyObj('server', ['listen', 'address'])
    fake_app = jasmine.createSpy('app')

    spyOn(http, 'createServer').and.returnValue(fake_server)
    spyOn(App, 'initialize').and.returnValue(fake_app)
    spyOn(config, 'getServerPort').and.returnValue(some_server_port)
    spyOn(config, 'getServerHost').and.returnValue(some_server_host)
    spyOn(config, 'getMongoUser').and.returnValue(some_mongo_user)
    spyOn(config, 'getMongoPassword').and.returnValue(some_mongo_password)
    spyOn(config, 'getMongoHost').and.returnValue(some_mongo_host)
    spyOn(config, 'getMongoPort').and.returnValue(some_mongo_port)
  })

  it('is initialized at the correct address', () => {
    const preventCallback = (..._args) => {}
    fake_server.listen.and.callFake(preventCallback)

    const server = Server.initialize({ config })

    expect(http.createServer).toHaveBeenCalledWith(fake_app)

    expect(server.listen).toHaveBeenCalledWith(
      some_server_port, some_server_host, jasmine.any(Function)
    )
  })

  it('logs the address to the console', () => {
    const fake_server_address = jasmine.createSpy()

    fake_server.address.and.returnValue(fake_server_address)

    fake_server.listen.and.callFake((...args) => {
      const [cb,] = args.slice().reverse()

      assert.func(cb, 'cb')

      spyOn(console, 'log')

      cb()

      expect(console.log).toHaveBeenCalledWith('Server listening at address: %j',
        fake_server_address)
    })

    const server = Server.initialize({ config })

    expect(http.createServer).toHaveBeenCalledWith(fake_app)

    expect(server.listen).toHaveBeenCalledWith(
      some_server_port, some_server_host, jasmine.any(Function)
    )
  })

  it('connects to the mongodb instance', () => {
    spyOn(Mongo, 'connect')

    Server.initialize({ config })

    expect(Mongo.connect).toHaveBeenCalledWith({
      user: some_mongo_user,
      password: some_mongo_password,
      host: some_mongo_host,
      port: some_mongo_port
    })
  })
})

