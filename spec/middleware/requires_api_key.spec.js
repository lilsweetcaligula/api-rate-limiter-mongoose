const assert = require('assert-plus')
const supertest = require('supertest')
const Express = require('express')
const Mongo = require('../support/mongo.js')
const Factories = require('../support/factories.js')
const ApiKey = require('../../src/models/api_key.js')
const ApiKeyLimiter = require('../../src/models/api_key_limiter.js')
const { requiresApiKey } = require('../../src/middleware/requires_api_key.js')

describe('API rate limiter', () => {
  const present = new Date(2019, 0, 1)

  beforeAll(() => {
    jasmine.clock().mockDate(present)
  })

  afterEach(async () => {
    await Mongo.clean()
  })

  const makeMockApp = (opts = {}) =>
    Express()
      .use(requiresApiKey(opts.middleware_opts))
      .use((req, res) => res.sendStatus(200))

  const requestProtectedResource = (mock_app = makeMockApp()) => {
    assert(mock_app, 'mock_app')
    return supertest(mock_app).get('/')
  }

  describe('when the "x-api-key" header is missing', () => {
    it('responds with 503 Service Unavailable', async () => {
      await requestProtectedResource().expect(503)
    })
  })

  describe('when the api key in the request is invalid', () => {
    const key = 'some_api_key'

    beforeEach(async () => {
      await Factories.createApiKey({ key: 'invalid_api_key' })
    })

    it('does not allow the request through', async () => {
      await requestProtectedResource()
        .set('x-api-key', key)
        .expect(503)
    })
  })

  describe('when the api key is valid', () => {
    let api_key

    const key = 'some_api_key'

    beforeEach(async () => {
      api_key = await ApiKey.create({ key })
    })

    describe('normally', () => {
      it('allows the request through', async () => {
        await requestProtectedResource()
          .set('x-api-key', key)
          .expect(200)
      })
    })

    describe('when the limit option is specified', () => {
      const mock_app = makeMockApp({ middleware_opts: { limit: 10 } })

      it('creates an ApiKeyLimiter document if such does not exist', async () => {
        expect(await ApiKeyLimiter.countDocuments()).toEqual(0)
        
        await requestProtectedResource(mock_app)
          .set('x-api-key', key)
          .expect(async (_res) => {
            expect(await ApiKeyLimiter.countDocuments()).toEqual(1)
          })
      })

      describe('when the ApiKeyLimiter already exists for the given API key', () => {
        beforeEach(async () => {
          await Factories.createApiKeyLimiter({ api_key_id: api_key.id })
        })

        it('does not create a new document', async () => {
          expect(await ApiKeyLimiter.countDocuments()).toEqual(1)
          
          await requestProtectedResource(mock_app)
            .set('x-api-key', key)
            .expect(async (_res) => {
              expect(await ApiKeyLimiter.countDocuments()).toEqual(1)
            })
        })
      })

      describe('when the ApiKeyLimiter already exists for a different API key', () => {
        const different_key = 'a9406vbaty'

        beforeEach(async () => {
          const another_api_key = await Factories
            .createApiKey({ key: different_key })

          await Factories.createApiKeyLimiter({
            api_key_id: another_api_key.id
          })
        })

        it('creates a new document', async () => {
          expect(await ApiKeyLimiter.countDocuments()).toEqual(1)
          
          await requestProtectedResource(mock_app)
            .set('x-api-key', key)
            .expect(async (_res) => {
              expect(await ApiKeyLimiter.countDocuments()).toEqual(2)
            })
        })
      })

      it('initializes the tracking attributes in the ApiKeyLimiter document', async () => {
        await requestProtectedResource(mock_app)
          .set('x-api-key', key)
          .expect(async (_res) => {
            const api_key_limiter = await ApiKeyLimiter.findOne()

            expect(api_key_limiter.api_key_id).toEqual(api_key._id)
            expect(api_key_limiter.start_count_at).toEqual(jasmine.any(Date))
            expect(api_key_limiter.num_calls).toEqual(1)
          })
      })

      describe('when the rate counting started less than an hour ago', () => {
        const less_than_an_hour_ago = present

        describe('when the count has exceeded the limit', () => {
          let api_key_limiter_id

          const mock_app = makeMockApp({ middleware_opts: { limit: 5 } })

          beforeEach(async () => {
            api_key_limiter_id = (await Factories.createApiKeyLimiter({
              api_key_id: api_key.id,
              start_count_at: less_than_an_hour_ago,
              num_calls: 7
            }))
          })

          it('responds with 503 Service Unavailable', async () => {
            await requestProtectedResource(mock_app)
              .set('x-api-key', key)
              .expect(503)
          })

          it('increments the counter anyway', async () => {
            await requestProtectedResource(mock_app)
              .set('x-api-key', key)
              .expect(async (_res) => {
                const api_key_limiter = await ApiKeyLimiter.findById(api_key_limiter_id)

                expect(api_key_limiter.start_count_at).toEqual(jasmine.any(Date))
                expect(api_key_limiter.num_calls).toEqual(8)
              })
          })
        })

        describe('when the count is less than the limit', () => {
          let api_key_limiter_id

          const mock_app = makeMockApp({ middleware_opts: { limit: 5 } })
            .use((err, req, res, next) => { console.error(err); next(err) })

          beforeEach(async () => {
            api_key_limiter_id = (await Factories.createApiKeyLimiter({
              api_key_id: api_key.id,
              start_count_at: less_than_an_hour_ago,
              num_calls: 3
            }))
          })

          it('allows the request through', async () => {
            await requestProtectedResource(mock_app)
              .set('x-api-key', key)
              .expect(200)
          })

          it('increments the counter', async () => {
            await requestProtectedResource(mock_app)
              .set('x-api-key', key)
              .expect(async (_res) => {
                const api_key_limiter = await ApiKeyLimiter.findById(api_key_limiter_id)

                expect(api_key_limiter.start_count_at).toEqual(jasmine.any(Date))
                expect(api_key_limiter.num_calls).toEqual(4)
              })
          })
        })
      })

      describe('when the rate counting started more than an hour ago', () => {
        const addTwoHours = to => new Date(to.getTime() + 2 * 60 * 60 * 1e3)
        const more_than_an_hour_ago = addTwoHours(present)

        describe('when the count has exceeded the limit', () => {
          let api_key_limiter_id

          const mock_app = makeMockApp({ middleware_opts: { limit: 5 } })

          beforeEach(async () => {
            api_key_limiter_id = (await Factories.createApiKeyLimiter({
              api_key_id: api_key.id,
              start_count_at: more_than_an_hour_ago,
              num_calls: 10
            }))
          })

          it('allows the request through', async () => {
            await requestProtectedResource(mock_app)
              .set('x-api-key', key)
              .expect(200)
          })

          it('resets the counter', async () => {
            await requestProtectedResource(mock_app)
              .set('x-api-key', key)
              .expect(async (_res) => {
                const api_key_limiter = await ApiKeyLimiter.findById(api_key_limiter_id)

                expect(api_key_limiter.start_count_at).toEqual(jasmine.any(Date))
                expect(api_key_limiter.num_calls).toEqual(1)
              })
          })
        })

        describe('when the count is less than the limit', () => {
          let api_key_limiter_id

          const mock_app = makeMockApp({ middleware_opts: { limit: 10 } })

          beforeEach(async () => {
            api_key_limiter_id = (await Factories.createApiKeyLimiter({
              api_key_id: api_key.id,
              start_count_at: more_than_an_hour_ago,
              num_calls: 5
            }))
          })

          it('allows the request through', async () => {
            await requestProtectedResource(mock_app)
              .set('x-api-key', key)
              .expect(200)
          })

          it('resets the counter', async () => {
            await requestProtectedResource(mock_app)
              .set('x-api-key', key)
              .expect(async (_res) => {
                const api_key_limiter = await ApiKeyLimiter.findById(api_key_limiter_id)

                expect(api_key_limiter.start_count_at).toEqual(jasmine.any(Date))
                expect(api_key_limiter.num_calls).toEqual(1)
              })
          })
        })
      })
    })
  })
})

