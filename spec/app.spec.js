const supertest = require('supertest')
const Mongo = require('./support/mongo.js')
const App = require('../src/app.js')
const Factories = require('./support/factories.js')
const ApiKey = require('../src/models/api_key.js')
const { uuidV4: matchesUuidV4 } = require('./support/matchers.js')(jasmine)

describe('App', () => {
  afterEach(async () => {
    await Mongo.clean()
  })

  describe('.initialize', () => {
    it('initializes the application', () => {
      const app = App.initialize()
      expect(app).toEqual(jasmine.any(Function))
    })
  })

  describe('creating an API key', () => {
    const requestCreateApiKey = () =>
      supertest(App.initialize())
        .post('/api_keys')

    describe('on success', () => {
      it('creates a new API key', async () => {
        expect(await ApiKey.countDocuments()).toEqual(0)

        await requestCreateApiKey()
          .expect(201)
          .expect(async (_resp) => {
            expect(await ApiKey.countDocuments()).toEqual(1)
          })
      })

      it('creates a new API key with a random uuid-v4 key', async () => {
        await requestCreateApiKey()
          .expect(201)
          .expect(async (_resp) => {
            const api_key = await ApiKey.findOne()

            expect(api_key.key).toEqual(matchesUuidV4())
          })
      })

      it('returns the id of the newly created api key', async () => {
        await requestCreateApiKey()
          .expect(201)
          .expect(async (res) => {
            expect(res.body).toEqual({ id: jasmine.any(String) })

            const { id: api_key_id } = res.body
            const api_key = await ApiKey.findById(api_key_id)

            expect(api_key).toEqual(jasmine.any(Object))
          })
      })
    })
  })
})

