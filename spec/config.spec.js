const config = require('../src/config.js')

describe('config', () => {
  describe('fromEnv', () => {
    const value = 'SOME_VALUE'

    beforeEach(() => {
      original_env = process.env
      process.env = { ...original_env, foo: value }
    })

    afterEach(() => {
      process.env = original_env
    })

    it('returns the env var if defined', () => {
      const result = config.fromEnv('foo')
      expect(result).toEqual(value)
    })

    it('throws an error if the value is not defined', () => {
      expect(() => config.fromEnv('bar'))
        .toThrow(new Error('process.env["bar"] is not defined'))
    })
  })

  describe('env var helpers', () => {
    const fake_value = 'FAKE_VALUE'

    beforeEach(() => {
      spyOn(config, 'fromEnv')
    })

    describe('getServerPort', () => {
      it('delegates to the "fromEnv" helper', () => {
        const env_var = 'SERVER_PORT'

        config.fromEnv.withArgs(env_var).and.returnValue(fake_value)

        const result = config.getServerPort()

        expect(config.fromEnv).toHaveBeenCalledWith(env_var)
        expect(result).toEqual(fake_value)
      })
    })

    describe('getServerHost', () => {
      it('delegates to the "fromEnv" helper', () => {
        const env_var = 'SERVER_HOST'

        config.fromEnv.withArgs(env_var).and.returnValue(fake_value)

        const result = config.getServerHost()

        expect(config.fromEnv).toHaveBeenCalledWith(env_var)
        expect(result).toEqual(fake_value)
      })
    })

    describe('getServerHostname', () => {
      it('delegates to the "fromEnv" helper', () => {
        const env_var = 'SERVER_HOSTNAME'

        config.fromEnv.withArgs(env_var).and.returnValue(fake_value)

        const result = config.getServerHostname()

        expect(config.fromEnv).toHaveBeenCalledWith(env_var)
        expect(result).toEqual(fake_value)
      })
    })

    describe('getMongoUser', () => {
      it('delegates to the "fromEnv" helper', () => {
        const env_var = 'MONGO_USER'

        config.fromEnv.withArgs(env_var).and.returnValue(fake_value)

        const result = config.getMongoUser()

        expect(config.fromEnv).toHaveBeenCalledWith(env_var)
        expect(result).toEqual(fake_value)
      })
    })

    describe('getMongoPassword', () => {
      it('delegates to the "fromEnv" helper', () => {
        const env_var = 'MONGO_PASSWORD'

        config.fromEnv.withArgs(env_var).and.returnValue(fake_value)

        const result = config.getMongoPassword()

        expect(config.fromEnv).toHaveBeenCalledWith(env_var)
        expect(result).toEqual(fake_value)
      })
    })

    describe('getMongoHost', () => {
      it('delegates to the "fromEnv" helper', () => {
        const env_var = 'MONGO_HOST'

        config.fromEnv.withArgs(env_var).and.returnValue(fake_value)

        const result = config.getMongoHost()

        expect(config.fromEnv).toHaveBeenCalledWith(env_var)
        expect(result).toEqual(fake_value)
      })
    })

    describe('getMongoPort', () => {
      it('delegates to the "fromEnv" helper', () => {
        const env_var = 'MONGO_PORT'

        config.fromEnv.withArgs(env_var).and.returnValue(fake_value)

        const result = config.getMongoPort()

        expect(config.fromEnv).toHaveBeenCalledWith(env_var)
        expect(result).toEqual(fake_value)
      })
    })
  })
})

