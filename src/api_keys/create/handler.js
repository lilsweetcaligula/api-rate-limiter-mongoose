const { asyncHandler } = require('../../lib/express_utils.js')
const ApiKey = require('../../models/api_key.js')
const uuid = require('uuid')

exports.initialize = () => asyncHandler(async (req, res) => {
  const api_key = await ApiKey.create({ key: uuid.v4() })

  res.status(201).json({ id: api_key._id })

  return
})
