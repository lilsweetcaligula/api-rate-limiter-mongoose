const assert = require('assert-plus')
const Express = require('express')
const CreateApiKey = require('./create/handler.js')

exports.initialize = () =>
  Express.Router()
    .post('/', CreateApiKey.initialize())

