exports.handleAppError = () => (err, req, res, next) => {
  if (res.headersSent) {
    next(err)
    return
  }

  /* NOTE: Please put your handling logic of custom errors
   * below this line.
   */

  next(err)

  return
}
