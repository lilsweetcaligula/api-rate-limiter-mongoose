const assert = require('assert-plus')
const { asyncHandler } = require('../lib/express_utils.js')
const ApiKey = require('../models/api_key.js')
const ApiKeyLimiter = require('../models/api_key_limiter.js')

exports.requiresApiKey = (opts = {}) => asyncHandler(async (req, res, next) => {
  const X_API_KEY = 'x-api-key'
  const { headers } = req

  if (X_API_KEY in headers) {
    const key = headers['x-api-key']
    const api_key = await ApiKey.findOne({ key })

    if (api_key) {
      if ('limit' in opts) {
        assert.number(opts.limit, 'opts.limit')

        const upsert = await ApiKeyLimiter.updateForApiKey(
          api_key,
          {},
          { $inc: { num_calls: 1 } },
          { upsert: true, new: true }
        )

        const now = new Date()


        let api_limiter_doc

        api_limiter_doc = upsert._doc

        if (isFirstCallLessThanHourAgo(api_limiter_doc, { now })) {
          if (isLimitExceeded(api_limiter_doc, opts.limit)) {
            res.sendStatus(503)
            return
          }
        } else {
          await ApiKeyLimiter.updateOne({
            id: api_limiter_doc.id,
            num_calls: upsert._doc.num_calls
          }, {
            start_count_at: now,
            num_calls: 1
          })
        }
      }

      next()

      return
    }
  }

  res.sendStatus(503)

  return
})

const isLessThanHourAgo = (to, from) => {
  const ms_diff = Math.abs(to.getTime() - from.getTime())
  const min_diff = ms_diff / 1e3 / 60.0

  return min_diff < 60.0
}

const isFirstCallLessThanHourAgo = (doc, params) => {
  assert.object(doc, 'doc')
  assert('start_count_at' in doc, 'doc.start_count_at')
  assert.object(params, 'params')
  assert.date(params.now, params.now)

  const { now } = params

  return isLessThanHourAgo(now, doc.start_count_at)
}

const isLimitExceeded = (doc, limit) => {
  assert.object(doc, 'doc')
  assert('num_calls' in doc, 'doc.num_calls')
  assert.number(limit, 'limit')

  return doc.num_calls > limit
}

