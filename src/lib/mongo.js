const assert = require('assert-plus')
const mongoose = require('mongoose')

const MongoUtils = {
  connect: async (params) => {
    const { user, password, host, port } = params

    assert.string(user, 'user')
    assert.string(password, 'password')
    assert.string(host, 'host')
    assert.number(Number(port), 'Number(port)')

    const conn_url = `mongodb://${user}:${password}@${host}:${port}`
    const conn_opts = { useNewUrlParser: true, useUnifiedTopology: true }

    await mongoose.connect(conn_url, conn_opts)

    return mongoose.connection
  }
}

module.exports = MongoUtils
