const mongoose = require('mongoose')

const ApiKeySchema = new mongoose.Schema({
  key: {
    type: String,
    required: true
  }
}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  }
})

module.exports = mongoose.model('ApiKey', ApiKeySchema)
