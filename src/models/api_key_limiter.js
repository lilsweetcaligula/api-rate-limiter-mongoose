const mongoose = require('mongoose')

const ApiKeyLimiterSchema = new mongoose.Schema({
  api_key_id: {
    type: mongoose.Types.ObjectId,
    required: true
  },
  start_count_at: {
    type: Date,
    default: new Date(0)
  },
  num_calls: {
    type: Number,
    required: true,
    default: 0
  }
}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  }
})

/* TODO: Consider moving the business operations below to a separate
 * module.
 *
 * --- 2020-04-18
 */

ApiKeyLimiterSchema.statics.updateForApiKey =
  function (api_key, filter_params, update_params, opts = {}) {
    const filter = { ...filter_params, api_key_id: api_key.id }
    const update = { ...update_params, api_key_id: api_key.id }

    return this.findOneAndUpdate(filter, update, opts)
  }

module.exports = mongoose.model('ApiKeyLimiter', ApiKeyLimiterSchema)
