const Express = require('express')
const ApiKeyApi = require('./api_keys/routes.js')
const { handleAppError } = require('./middleware/handle_app_error.js')

exports.initialize = () =>
  Express()
    .use(Express.json())
    .use('/api_keys', ApiKeyApi.initialize())
    .use(handleAppError())
